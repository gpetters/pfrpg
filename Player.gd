extends KinematicBody

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var direction = Vector3()

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	direction = Vector3()
	if Input.is_key_pressed(KEY_W):
		direction.x = 1
	elif Input.is_key_pressed(KEY_S):
		direction.x = -1
	if Input.is_key_pressed(KEY_A):
		direction.z = -1
	elif Input.is_key_pressed(KEY_D):
		direction.z = 1
	direction = direction.normalized()
	move_and_slide(direction, Vector3(0, 1, 0))